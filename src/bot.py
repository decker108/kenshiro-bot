# -*- coding: utf-8 -*-
import logging
from logging.handlers import RotatingFileHandler
from os import environ
from discord.ext import commands

import sentry_sdk
sentry_sdk.init(dsn=environ.get('SENTRY_DSN_KENSHIROBOT', ''))


def _setupLogging():
    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

    discordLogger = logging.getLogger("kenshirobot")
    discordLogger.setLevel(logging.INFO)

    logfilePath = environ.get("KENSHIROBOT_LOG_PATH", "/var/log/kenshirobot/app.log")
    rfh = RotatingFileHandler(logfilePath, maxBytes=1000000, backupCount=2)
    rfh.setFormatter(formatter)

    strh = logging.StreamHandler()
    strh.setFormatter(formatter)

    discordLogger.addHandler(rfh)
    discordLogger.addHandler(strh)
    return discordLogger


logger = _setupLogging()


def setupBot():
    bot = commands.Bot(command_prefix='/', description='Kenshirobot knows all about Fist of the Northstar')

    @bot.command()
    async def recommend(ctx):
        await ctx.send('Based on your request, I found this recommendation: '
                       'https://myanimelist.net/anime/967/Hokuto_no_Ken')

    return bot


def main():
    bot = setupBot()
    logger.info("Starting bot")
    try:
        bot.run(environ['DISCORD_API_TOKEN'])
    except Exception as e:
        logger.exception("Unexpected error in main loop", exc_info=e)


if __name__ == '__main__':
    main()
